def contar_caracteres(texto: str) -> dict:
    """

    >>> contar_caracteres('hola mundo')
    {'h':1, 'o':2, 'l':1, 'a':1, ' ':1, 'm':1, 'u':1, 'n':1, 'd': 1}

    """
