diccionario = {'saludo': 'hola mundo',
               'clase': 'grupo 79', 'numero_estudiantes': 32}

print(diccionario)

print(
    f'el numero de estudiantes en mi clase es {diccionario["numero_estudiantes"]}')

diccionario["numero_estudiantes"] += 10

print(
    f'el numero de estudiantes en mi clase es {diccionario["numero_estudiantes"]}')

diccionario['asistencia'] = [20, 20, 12, 17]

print(diccionario)
