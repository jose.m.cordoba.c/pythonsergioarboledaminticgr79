"""Progrmaa que valida si un numero ingresado por el usuario es menor que 10
"""

numero = 11

# Mientras el numero sea mayor que 10
while numero > 10:
    numero = float(input('Por favor ingrese un numero < 10 '))

print(f'Felicidades su numero es {numero}')
