"""Progrmaa que valida si un numero ingresado por el usuario es menor que 10
"""

# Inicializar para que se cumpla mi while
numero = float(input('Por favor ingrese un numero entre 0 y 20 '))

# Mientras el numero sea mayor que 10
while numero > 20 or numero < 0:
    numero = float(input('Por favor ingrese un numero entre 0 y 20 '))

print(f'Felicidades su numero es {numero}')
