"""Permite obtener los elementos deseados en la secuancia fibonacci
"""


def fibonacci(elemento: int) -> int:
    """

    >>> fibonacci(0)
    0

    >>> fibonacci(5)
    5

    >>> fibonacci(6)
    8

    >>> fibonacci(7)
    13

    """
    contador = 0
    actual, siguiente = 0, 1

    while contador < elemento:
        actual, siguiente = siguiente, siguiente + actual
        contador += 1

    return actual


while True:
    control = input(
        'Ingrese un numero en la serie de fibonacci o "S" para salir ')
    if control.lower() == 's':
        break
    else:
        control = int(control)
        fib_n = fibonacci(control)
        print(
            f'El elemento de la serie de fibonacci en la posición {control} es {fib_n}')
