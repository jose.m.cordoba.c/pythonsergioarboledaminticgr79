"""Programa que suma los numeros ingrsados por el usuario hasta que escribe un 0
"""
acumulador = 0
numero = float(input('Ingrese un número (0 para salir)'))
while numero != 0:
    acumulador += numero
    numero = float(input('Ingrese un número (0 para salir)'))

print(f'El acumulado es {acumulador}')
