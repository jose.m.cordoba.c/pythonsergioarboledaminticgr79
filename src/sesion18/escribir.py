# Abrir el archivo
# 'w' indica el modo de escritura
archivo = open('src/sesion18/ejemplo.txt', 'w')

# Procesar el archivo
for i in range(10):
    archivo.write(f'Esta es la linea numero {i}\n')

# cerrar el archivo
archivo.close()
