# Abrir el archivo
# 'a' indica el modo de agregar
archivo = open('src/sesion18/ejemplo.txt', 'a')

archivo.write('Nueva data \n')
# Procesar el archivo
for i in range(10, 20):
    archivo.write(f'Esta es la linea numero {i}\n')

# cerrar el archivo
archivo.close()
