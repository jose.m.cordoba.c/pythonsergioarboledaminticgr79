"""Este programa pregunta los datos del usuario y calcula el salario
    """

# Leer Datos
nombre_usuario = input("Por favor digite su nombre ")
identificación = input("Por favor digite su identificación ")
horas_trabajadas = input("Ingrese las horas trabajadas ")
horas_trabajadas = int(horas_trabajadas)
valor_hora = input("Ingrese el valor de la hora trabajada ")
valor_hora = int(valor_hora)

# Proceso
salario = horas_trabajadas * valor_hora

# Salida
print(f'''El usuario: {nombre_usuario},
Identificado con: {identificación},
Que trabajó: {horas_trabajadas} horas,
Con un valor de: ${valor_hora},
Obtuvo un salario de: ${salario}
''')
