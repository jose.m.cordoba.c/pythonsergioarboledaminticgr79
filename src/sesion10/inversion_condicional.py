"""Programa que decide sidebemos reinvertir la inversion
    """

tasa, capital = input(
    'Por favor ingrese la tasa de interes y el capital a invertir separado por espacios ').split()
tasa = float(tasa)
capital = float(capital)

rendimiento = tasa * capital

capital_final = capital

# Si el redimiento supera los 7000 entonces reinvertimos
if rendimiento > 7000:
    capital_final += rendimiento

print(f'El capital final para esta inversión es {capital_final}')
