"""Determina el valor a pagar por concepto de horas extra
    """

HORAS_NORMALES = 40


def valor_a_pagar_extra(horas_trabajadas: int, valor_hora: float) -> float:
    """

    >>> valor_a_pagar_extra(40, 10)
    0

    >>> valor_a_pagar_extra(48, 10)
    160

    >>> valor_a_pagar_extra(50, 10)
    220

    """
    cantidad_horas_extra = horas_trabajadas - HORAS_NORMALES
    valor_a_pagar = 0
    if cantidad_horas_extra < 0:
        valor_a_pagar = 0
    elif cantidad_horas_extra <= 8:
        valor_a_pagar = cantidad_horas_extra * valor_hora * 2
    else:
        horas_triple = cantidad_horas_extra - 8
        # valor_a_pagar = 8 * valor_hora * 2 + horas_triple * valor_hora * 3
        valor_a_pagar = valor_hora * (16 + 3 * horas_triple)
    return valor_a_pagar
