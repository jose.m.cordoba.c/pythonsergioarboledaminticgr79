"""programa que hace operaciones dependiendo los valores de las entradas
    """


def operaciones(num1, num2) -> float:
    """

    >>> operaciones(2, 2)
    4

    >>> operaciones(7, 2)
    5

    >>> operaciones(3, 6)
    9

    """
    if num1 < num2:
        return num1 + num2
    elif num1 == num2:
        return num1 * num2
    else:
        return num1 - num2
