"""Programa para calcular el redimiento mensual de una inversión
    """

TASA_EFECTIVA_ANUAL = 0.15


def convertir_tasa_EA_NMV(tasa: float) -> float:
    """Convierte una tasa efectiva anual en nominal mes vencido

    >>> convertir_tasa_EA_NMV(0.15)
    0.01171491691985338

    Args:
        tasa (float): La tasa efectiva anual

    Returns:
        float: la tasa nominal mes vencido
    """
    tasa_nmv = (1 + tasa) ** (1 / 12) - 1
    return tasa_nmv


def calcular_rendimiento_mensual(valor_inversion: float, tasa: float = TASA_EFECTIVA_ANUAL) -> float:
    """Calcula el rendimiento de una inversion dada la tasa 

    >>> calcular_rendimiento_mensual(100)
    1.17

    >>> calcular_rendimiento_mensual(0)
    0.0

    >>> calcular_rendimiento_mensual(100, 0.2)
    1.53

    Args:
        valor_inversion (float): El valor a invertir
        tasa (float, optional): La tasa de interes efectiva anual. Defaults to TASA_EFECTIVA_ANUAL.

    Returns:
        float: el rendimiento de la inversion
    """
    rendimiento_mensual = valor_inversion * \
        convertir_tasa_EA_NMV(tasa)
    return round(rendimiento_mensual, 2)
