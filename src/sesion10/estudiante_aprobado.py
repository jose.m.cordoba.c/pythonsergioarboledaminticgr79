"""Programa para determinar si un estudiante aprueba dados sus resultados
    """


def aprobo(calificacion1: float, calificacion2: float, calificacion3: float) -> bool:
    """Determina si un estudiante aprueba basado en sus calificaciones

    >>> aprobo(70, 70, 70)
    True

    >>> aprobo(69, 70, 70)
    False


    Args:
        calificacion1 (float): [description]
        calificacion2 (float): [description]
        calificacion3 (float): [description]

    Returns:
        bool: [description]
    """

    promedio = (calificacion1 + calificacion2 + calificacion3) / 3
    if promedio >= 70:
        return True
    else:
        return False
