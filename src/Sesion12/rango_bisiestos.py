from bisiesto import es_bisiesto, es_divisible

desde, hasta = input(
    'ingrese el rango de años a evaluar separado por espacio ').split()
desde, hasta = int(desde), int(hasta)

for año in range(desde, hasta):
    if es_divisible(año, 10) and es_bisiesto(año):
        print(f'El año {año} es bisiesto y divisible entre 10')
