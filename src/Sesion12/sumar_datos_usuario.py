n = int(input('Por favor ingrese la cantidad de datos a trabajar '))

acumulador = 0

for i in range(n):
    acumulador += float(input(f'{i+1}. Por favor ingrese un dato: '))

print(f'El total acumulado es {acumulador}')
