def contar_vocales(texto: str) -> int:
    """

    >>> contar_vocales('hola mundo')
    4

    >>> contar_vocales('Anita Lava La Tina')
    8

    >>> contar_vocales('nt Lv L Tn')
    0

    """
    contador = 0
    for letra in texto:
        if es_vocal(letra):
            contador += 1
    return contador


# TIP: hacer una funcion es_vocal
def es_vocal(letra: str) -> bool:
    """
    >>> es_vocal('A')
    True

    >>> es_vocal('i')
    True

    >>> es_vocal('Ua')
    False

    >>> es_vocal('Z')
    False

    >>> es_vocal('r')
    False

    """
    return len(letra) == 1 and letra in 'AEIOUaeiou'
