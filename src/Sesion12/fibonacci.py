"""Imprime los primeros 10 elementos de la sucesión de fibonacci"""

actual, siguiente = 0, 1

for i in range(10):
    print(actual)
    actual, siguiente = siguiente, actual + siguiente
