def es_divisible(numero: int, divisor: int) -> bool:
    """

    >>> es_divisible(27, 3)
    True

    >>> es_divisible(73, 3)
    False

    """
    return numero % divisor == 0


def es_bisiesto(ano: int) -> bool:
    """

    >>> es_bisiesto(100)
    False

    >>> es_bisiesto(400)
    True

    >>> es_bisiesto(1997)
    False

    >>> es_bisiesto(1996)
    True

    """
    return es_divisible(ano, 400) or (es_divisible(ano, 4) and not es_divisible(ano, 100))
