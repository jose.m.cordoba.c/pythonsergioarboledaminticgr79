"""Obtiene el factorial de un número"""


def factorial(n: int) -> int:
    """Calcula el facotrial de n

    >>> factorial(0)
    1

    >>> factorial(3)
    6

    >>> factorial(100)
    93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000

    Args:
        n (int): [description]

    Returns:
        int: [description]
    """
    resultado = 1
    for i in range(1, n + 1):
        resultado *= i
    return resultado
