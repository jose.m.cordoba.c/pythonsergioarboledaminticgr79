"""Ejemplos del ciclo for"""

print('Usar for para imprimir los numeros del 0 al 9')
for numero in range(10):
    print(numero)


print('Usar for para imprimir en el rango -5 al 30')
for valor in range(-5, 31):
    print(valor)


print('Todos los pares entre 14 y 28')
for valor in range(14, 29, 2):
    print(valor)

print('Todos los impares entre 13 y 30')
for numero in range(13, 30, 2):
    print(numero)

print('Todos los multiplos de 7 hasta 140')
for entero in range(7, 141, 7):
    print(entero)

print('Los numeros desde 10 al 1')
for numero in range(10, 0, -1):
    print(numero)
