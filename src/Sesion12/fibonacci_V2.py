"""Imprime los primeros 10 elementos de la sucesión de fibonacci"""


def fibonacci(n: int) -> int:
    """Retorna el elemento deseado en la serie fibonacci

    >>> fibonacci(10)
    34

    >>> fibonacci(1)
    0

    >>> fibonacci(2)
    1

    >>> fibonacci(3)
    1

    Args:
        n (int): [description]

    Returns:
        int: [description]
    """

    actual, siguiente = 0, 1

    for i in range(n - 1):
        actual, siguiente = siguiente, actual + siguiente

    return actual
