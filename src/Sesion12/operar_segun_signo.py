suma_negativos = 0
promedio_positivos = 0
contador_positivos = 0

for i in range(6):
    dato = int(input('Por favor ingrese un numero '))
    if dato < 0:
        suma_negativos += dato
    else:
        promedio_positivos += dato
        contador_positivos += 1

print(f'la suma de negativos es {suma_negativos}')
if contador_positivos == 0:
    print('no hubo numeros positivos')
else:
    promedio_positivos /= contador_positivos
    print(f'El promedio de los numeros positivos es {promedio_positivos}')
