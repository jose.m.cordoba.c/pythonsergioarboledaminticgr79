def poker(c1: tuple, c2: tuple, c3: tuple, c4: tuple, c5: tuple) -> bool:
    """

    >>> poker(("♥️", 7), ("♠️", 7), ("♣️" ,7), ("♦️",7), ("♣️",5))
    True

    >>> poker(("♥️", 5), ("♠️", 7), ("♣️" ,7), ("♦️",7), ("♣️",5))
    False

    >>> poker(("♥️", 7), ("♠️", 7), ("♣️" ,1), ("♦️",7), ("♣️",7))
    True

    """
    return poker_small(c1, c2, c3, c4) \
        or poker_small(c2, c3, c4, c5) \
        or poker_small(c3, c4, c5, c1) \
        or poker_small(c3, c2, c5, c1) \
        or poker_small(c3, c4, c5, c1) \
        or poker_small(c1, c2, c4, c5) \



def poker_small(c1: tuple, c2: tuple, c3: tuple, c4: tuple) -> bool:
    """

    >>> poker_small(("♥️", 7), ("♠️", 7), ("♣️" ,7), ("♦️",7))
    True

    >>> poker_small(("♥️", 7), ("♠️", 7), ("♣️" ,7), ("♦️",5))
    False

    """
    return (c1[1] == c2[1] == c3[1] == c4[1]) and (c1[0] != c2[0] != c3[0] != c4[0])
