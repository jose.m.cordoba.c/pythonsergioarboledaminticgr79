# Algunos ejemplos de listas

lista_vacia = []
print(lista_vacia)

lista_con_un_elemento = ['Hola']
print(lista_con_un_elemento)

lista_con_varios = ['Hola', 10, True, 7.5, 'Mundo']
print(lista_con_varios)

# Acceder a un elemento de la lista
print(lista_con_varios[1])
print(lista_con_varios[-1])

# Obtener la cantidad de elementos de la lista
print(len(lista_con_varios))

# Agregando un elemento a la lista
lista_con_varios.append(123)
print(lista_con_varios)

# Insertando un elemento en una poscicion
lista_con_varios.insert(3, 321)
print(lista_con_varios)

# podemos eliminar elementos de la lista con remove
lista_con_varios.remove(123)
print(lista_con_varios)

numeros_enteros = list(range(0, 10))
print(numeros_enteros)

# Slicing o porciones
print(numeros_enteros[0:5])
print(numeros_enteros[:5])

print(numeros_enteros[5:7])

# invertir una lista
print(numeros_enteros[::-1])
print(numeros_enteros[-3:-5:-1])

# recorrer listas
lista_pares = []
for elemento in numeros_enteros:
    if elemento % 2 == 0:
        lista_pares.append(elemento)

print(lista_pares)

lista_pares = []
for i in range(len(numeros_enteros)):
    if numeros_enteros[i] % 2 == 0:
        lista_pares.append(numeros_enteros[i])

print(lista_pares)

matriz = [[1, 2, 3], [1, 2, 3], [3, 4, 5, 6, 7]]

for fila in matriz:
    print(f'la fila es {fila}')
    for elemento in fila:
        if elemento % 2 == 1:
            print(elemento)


lista_pares[-1] = 100
print(lista_pares)
