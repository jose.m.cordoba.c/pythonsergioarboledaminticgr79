"""Programa para establecer el precio final de un producto
    """

PORCENTAJE_UTILIDAD = 1.2
PORCENTAJE_IMPUESTOS = 0.15


def precio_final(costo_produccion) -> float:
    """Calcula el precio final dado el costo de produccion

    >>> precio_final(100)
    253.0

    >>> precio_final(0)
    0.0

    >>> precio_final(1567)
    3964.5099999999993


    Args:
        costo_produccion (float): el costo de producion de un elemento

    Returns:
        float: El precio final incluyendo utlidad e impuestos
    """
    utilidad = costo_produccion * PORCENTAJE_UTILIDAD
    precio_antes_impuesto = costo_produccion + utilidad
    impuesto = precio_antes_impuesto * PORCENTAJE_IMPUESTOS
    precio_venta = precio_antes_impuesto + impuesto
    return precio_venta
