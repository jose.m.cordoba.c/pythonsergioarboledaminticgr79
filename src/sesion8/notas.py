"""Programa para calcular la nota final de un estudiante
"""

# CONSTANTES DEL PROBLEMA
PORCENTAJE_NOTA_TALLERES = 0.3
PORCENTAJE_NOTA_QUIZES = 0.3
PORCENTAJE_NOTA_PROYECTO_FINAL = 0.4


def calcular_nota_definitiva(taller_1, taller_2, taller_3,
                             quiz_1, quiz_2,
                             trabajo, sustentacion) -> float:
    """Calcula la nota definitiva de un estudiante dados sus notas 
    parciales

    >>> calcular_nota_definitiva(0, 0, 0, 0, 0, 0, 0)
    0.0

    >>> calcular_nota_definitiva(10, 10, 10, 10, 10, 10, 10)
    10.0

    >>> calcular_nota_definitiva(10, 10, 10, 0, 0, 0, 0)


    Args:
        taller_1 (float): La nota del primer taller, los talleres valen el 30% de la nota final
        taller_2 (float): La nota del segundo taller, los talleres valen el 30% de la nota final
        taller_3 (float): La nota del taller taller, los talleres valen el 30% de la nota final
        quiz_1 (float): La nota del primer quiz, los quizes valen 30% de la nota final
        quiz_2 (float): La nota del segundo quiz, los quizes valen 30% de la nota final
        trabajo (float): [description]
        sustentacion (float): [description]

    Returns:
        float: [El resultado de la nota final]
    """

    # Calcular la nota de talleres
    nota_talleres = (taller_1 + taller_2 + taller_3) / 3  # Promedio talleres
    nota_talleres *= PORCENTAJE_NOTA_TALLERES

    # Calcular la nota de quizes
    nota_quizes = (quiz_1 + quiz_2) / 2 * PORCENTAJE_NOTA_QUIZES

    # Calcular la nota del trabajo final
    nota_trabajo = (trabajo + sustentacion) / 2 * \
        PORCENTAJE_NOTA_PROYECTO_FINAL

    # retornar la suma de las notas
    return nota_talleres + nota_quizes + nota_trabajo
