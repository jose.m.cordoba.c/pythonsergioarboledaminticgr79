productos = {
    1: ['manzanas', 8000.0, 550],
    2: ['limones', 2300.0, 15],
    3: ['peras', 2500.0, 38],
    4: ['arandanos', 9300.0, 55],
    5: ['tomates', 2100.0, 42],
    6: ['fresas', 4100.0, 33],
    7: ['helado', 4500.0, 41],
    8: ['galletas', 500.0, 833],
    9: ['chocolates', 3900.0, 999],
    10: ['jamon', 17000.0, 10]
}


def agregar(codigo: int, nombre: str, precio: float, cantidad: int):
    pass


def actualizar(codigo: int, nombre: str, precio: float, cantidad: int):
    pass


def borrar(codigo: int, nombre: str, precio: float, cantidad: int):
    pass


def encontrar_producto_mayor_precio() -> str:
    pass


def encontrar_producto_menor_precio() -> str:
    pass


def calcular_promedio_precios() -> float:
    pass


def calcular_valor_inventario() -> float:
    pass


# Entrada
operacion = input()
nombre, precio, cantidad = input().split()
precio = float(precio)
cantidad = int(cantidad)

# Proceso
# decidir que operacion se solicitó y llamar la funcion correspondiente


# Salida
# TODO ojo solo si no se produjeron errores
print(encontrar_producto_mayor_precio(), encontrar_producto_menor_precio(),
      calcular_promedio_precios(), calcular_valor_inventario())
