# Entrada datos
distancia_metros, velocidad_maxima_km_hora, tiempo_segundos = input().split()

# Proceso


def convertir_metros_en_km(metros: float) -> float:
    """

    >>> convertir_metros_en_km(1000)
    1

    >>> convertir_metros_en_km(250)
    0.25

    """


def convertir_segundos_en_horas(segundos: float) -> float:
    """

    >>> convertir_segundos_en_horas(3600)
    1.0

    """


def construir_mensaje_salida(velocidad_record: float, velocidad_leida: float) -> str:
    """

    >>> construir_mensaje_salida( -100, -80)
    'MEDICION ERRONEA'

    >>> construir_mensaje_salida( 100, 80)
    'VELOCIDAD NORMAL'

    >>> construir_mensaje_salida( 100, 110)
    'NUEVO RECORD'

    >>> construir_mensaje_salida( 100, 120)
    'ENTREVISTA'


    """
    # Calcular el 115% de la velocidad record
    # Si los datos son menores que 0 es un error
    # Si la velocidad record es mayor a la leida
    # Si es menor que el 115%
    # Si es mayor o igual que el 115%


def calcular_velocidad(distancia: float, tiempo: float) -> float:
    """

    >>> calcular_velocidad(250, 2)
    125

    >>> calcular_velocidad(90, 3)
    30

    """


# Convertir las unidades
# Calcular la velocidad
# generar mensage

# Salida Imprimir texto segun condición
