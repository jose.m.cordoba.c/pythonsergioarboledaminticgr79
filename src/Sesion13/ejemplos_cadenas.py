texto = 'hola mundo'

# Obtener una letra
print(texto[0])

print('imprimir letra por letra')
for letra in texto:
    print(letra)

# Puedo leer de derecha a izquierda
print(f' la ultima letra de mi texto es: "{texto[-1]}"')

print('este es un ' + 'texto concatenado ')

print(f'Mi texto invertido es "{texto[::-1]}"')

# Multiplicar texto
print(texto * 3)

# Obteniendo una porcion de la cadena
print(texto[0:5])
print(texto[5:])

# contar caracteres
print(texto.count('o'))

# buscar el indice
print(texto.find('mu'))

# dividir por espacios
print(texto.split())
