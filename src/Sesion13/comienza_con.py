def comienza_con(texto: str, otro_texto: str) -> bool:
    """

    >>> comienza_con('hola mundo', 'hola')
    True

    >>> comienza_con('hola mundo', 'hola mundo')
    True

    >>> comienza_con('hola mundo', 'mundo')
    False

    >>> comienza_con('hola mundo', 'Jose')
    False

    """
    return 0 == texto.find(otro_texto)


def contar_palabras(texto: str) -> int:
    """

    >>> contar_palabras('hola mundo')
    2

    >>> contar_palabras('')
    0

    >>> contar_palabras('anita lava la tina')
    4

    """
    return len(texto.split())
