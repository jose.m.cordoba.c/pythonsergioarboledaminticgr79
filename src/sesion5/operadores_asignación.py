"""Programa de ejmplo de operaciones areitméticas

Capturarenmos 2 numeros y haremos todas las operaciones asociadas
"""

# Leemos los numeros
numero1, numero2 = input('Digita dos números separados por espacio ').split()

# Convertimos los valores de texto a numero
numero1 = float(numero1)
numero2 = float(numero2)

# asignación
acumulador = numero1
print(f'Mi acumulador tiene el valor de: {acumulador}')

input("Que valor tiene mi acumulador? ")

# asignación con suma
acumulador += numero2
print(f'Mi acumulador tiene el valor de: {acumulador}')

input("Que valor tiene mi acumulador? ")

# asignación con resta
acumulador -= numero1
print(f'Mi acumulador tiene el valor de: {acumulador}')

# asignacion con *
# asignacion con /
# asignacion con %
