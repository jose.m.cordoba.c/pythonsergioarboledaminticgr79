"""Programa con ejemplos de operadores de comparación
    """

numero1 = 324
numero2 = 123

print(f'¿{numero1} es igual a {numero2}?')
# Miramos si mis numeros son iguales
print(numero1 == numero2)

print(f'¿{numero1} es diferente a {numero2}?')
# Miramos si mis numeros son diferentes
print(numero1 != numero2)

print(f'¿{numero1} es mayor a {numero2}?')
# Miramos si mi primer nuemro es mayor
print(numero1 > numero2)

print(f'¿{numero1} es menor a {numero2}?')
# Miramos si mi primer nuemro es mayor
print(numero1 < numero2)

print(f'¿{numero1} es mayor o igual a {numero2}?')
# Miramos si mi primer nuemro es mayor
print(numero1 >= numero2)

print(f'¿{numero1} es menor o igual a {numero2}?')
# Miramos si mi primer nuemro es mayor
print(numero1 <= numero2)
