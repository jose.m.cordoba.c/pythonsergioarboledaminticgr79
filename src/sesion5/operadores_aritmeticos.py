"""Programa de ejmplo de operaciones areitméticas

Capturarenmos 2 numeros y haremos todas las operaciones asociadas
"""

# Leemos los numeros
numero1, numero2 = input('Digita dos números separados por espacio ').split()

# Convertimos los valores de texto a numero
numero1 = float(numero1)
numero2 = float(numero2)

suma = numero1 + numero2
resta = numero1 - numero2
producto = numero1 * numero2
division = numero1 / numero2
division_entera = numero1 // numero2
modulo = numero1 % numero2

# Redondeos
division = round(division, 2)


print(f'''Para los numeros {numero1} y  {numero2}
El resultado de la adicion es: {suma}
El resultado de la sustraccion es: {resta}
El resultado de la multiplicación es: {producto}
El resultado de la division es: {division}
El resultado de la division entera es: {division_entera}
Y el residuo es: {modulo}
''')
