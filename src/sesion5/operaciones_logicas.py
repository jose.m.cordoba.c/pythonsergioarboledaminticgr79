"""Ejemplo de operaciones lógicas
    """

verdad = True
mentira = False

print(
    f'El tipo de datos de la verdad y la mentira es: {type(verdad)} y {type(mentira)}')

# Negacion
print(f'La negacion de la verdad es {not verdad}')
print(f'La negacion de la mentira es {not mentira}')

# Conjuncion "y"
print(f'La conjuncion entra la verdad y la mentira {verdad and mentira}')

# Disyunción "o"
print(f'La disyuncion entra la verdad y la mentira {verdad or mentira}')
