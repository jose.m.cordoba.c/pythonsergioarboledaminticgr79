"""Ejemplos de operaciones de conversion
    """

texto = '789'
print(f'Mi variable con el valor de "{texto}" es de tipo {type(texto)}')

entero = int(texto)
print(f'Mi variable con el valor de "{entero}" es de tipo {type(entero)}')

numero_de_punto_flotante = float(texto)
print(
    f'Mi variable con el valor de "{numero_de_punto_flotante}" es de tipo {type(numero_de_punto_flotante)}')

texto = str(numero_de_punto_flotante)
print(f'Mi variable con el valor de "{texto}" es de tipo {type(texto)}')
